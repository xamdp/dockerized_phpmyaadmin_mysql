# Task: Connect phpMyAdmin to MySQL Docker Containers

This task involves setting up a development environment where you can manage a 
**MySQL** database through a **phpMyAdmin** web interface using Docker containers.

## Objective:
- Launch separate Docker containers for phpMyAdin and MySQL
- Configure the phpMyAdmin container to conect to the running MySQL container.
- Access the phpMyAdmin interface to manage your MySQL databases.
